package edu.uprm.ece.icom4035.list;

public class ArrayListFactory<E> implements ListFactory<E> {
	
	public List<E> getList(String criteria) {
		if(criteria.equals("array")) {
			return newInstance();
		}
		return null;
	}

	@Override
	public List<E> newInstance() {
		return new ArrayList<E>();
	}

}
