package edu.uprm.ece.icom4035.list;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArrayList<E> implements List<E> {

	private class ArrayListIterator<E> implements Iterator<E> {
		private int curr;

		public ArrayListIterator() {
			this.curr = 0;
		}
		@Override
		public boolean hasNext() {
			return this.curr < size();
		}
		@SuppressWarnings("unchecked")
		@Override
		public E next() {
			if(this.hasNext()) {
				E result = (E) elements[this.curr];
				curr++;
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}

	}

	private static final int ICAP = 10;

	private E[] elements;
	private int size;

	public ArrayList() {
		this.elements = (E[]) new Object[ICAP];
		this.size = 0;
	}
	public ArrayList(int initCapacity) {
		if (initCapacity < 1) {
			throw new IllegalArgumentException
			("Initial capacity must be at least 1");
		}
		this.elements= (E[]) new Object[initCapacity];
		this.size = 0;
	}	

	public int capacity() { 
		return elements.length; 
	}

	private void changeCapacity(int change) { 
		E[] newElement = (E[]) new Object[change]; 
		for (int i=0; i<size; i++) { 
			newElement[i] = this.elements[i];
		} 
		this.elements = newElement; 
	}

	@Override
	public Iterator<E> iterator() {
		return new ArrayListIterator<E>();
	}

	@Override
	public void add(E obj) {
		if (this.size() == this.elements.length) {
			this.changeCapacity(2*this.size());
		}
		this.elements[this.size] = obj;
		this.size++;
	}

	@Override
	public void add(int index, E obj) {
		if(index < 0 || index > this.size) {
			throw new IndexOutOfBoundsException("Invalid index");
		}
		if (this.size() == this.elements.length) {
			this.changeCapacity(2*this.size());
		}
		if(index == this.size) {
			this.add(obj);
			return;
		}
		for(int i = this.size; i > index; i--) {
			this.elements[i] = this.elements[i-1];
		}
		this.elements[index] = obj;
		this.size++;
	}

	@Override
	public boolean remove(E obj) {
		int index = this.firstIndex(obj);

		if(this.isEmpty() || index == -1) {
			return false;
		}
		for(int i = index; i <this.size-1; i++) {
			this.elements[i] = this.elements[i+1];
		}
		this.elements[this.size-1] = null;
		this.size--;

		return true;
	}

	@Override
	public boolean remove(int index) {
		if(index < 0 || index >= this.size) {
			throw new IndexOutOfBoundsException("Index is invalid");
		}
		for(int i = index; i <this.size-1; i++) {
			this.elements[i] = this.elements[i+1];
		}
		this.elements[this.size-1] = null;
		this.size--;

		return true;
	}

	@Override
	public int removeAll(E obj) {
		if(this.isEmpty()) {
			return 0;
		}
		int countRemoved = 0;
		int index = this.firstIndex(obj);
		for(int i = index; i <=this.size; i++) {
			if(this.elements[i] == obj) {
				remove(i);
				i--;
				countRemoved++;
			}
		}
		return countRemoved;
	}

	@Override
	public E get(int index) {
		if(index < 0 || index >= this.size) {
			throw new IndexOutOfBoundsException("Index is invalid");
		}
		return this.elements[index];
	}

	@Override
	public E set(int index, E obj) {
		if(index < 0 || index >= this.size) {
			throw new IndexOutOfBoundsException("Index is invalid");
		}
		E etr = this.elements[index];
		this.elements[index] = obj;
		return etr;
	}

	@Override
	public E first() {
		if(isEmpty()) {
			return null;
		}
		return this.elements[0];
	}

	@Override
	public E last() {
		if(isEmpty()) {
			return null;
		}
		return this.elements[this.size-1];
	}

	@Override
	public int firstIndex(E obj) {
		for(int i = 0; i< this.size-1; i++) {
			if(this.elements[i] == obj) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public int lastIndex(E obj) {
		for(int i = this.size-1; i > 0; i--) {
			if(this.elements[i] == obj) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public int size() {
		return this.size;
	}

	@Override
	public boolean isEmpty() {
		return this.size == 0;
	}

	@Override
	public boolean contains(E obj) {
		for(int i = 0; i <this.size; i++) {
			if(this.elements[i] == obj) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void clear() {
		for(int i = 0; i < this.size; i++) {
			this.elements[i] = null;
		}
	}

}
