package edu.uprm.ece.icom4035.list;

import java.util.Iterator;
import java.util.NoSuchElementException;



public class SinglyLinkedList<E> implements List<E> {

	private class SinglyLinkedListIterator<E> implements Iterator<E> {
		private Node<E> curr;
		
		@SuppressWarnings("unchecked")
		public SinglyLinkedListIterator() {
			this.curr = (Node<E>) dummy.getNext();
		}
		
		@Override
		public boolean hasNext() {
			return this.curr != null;
		}

		@Override
		public E next() {
			if(this.hasNext()) {
				E result = this.curr.getElement();
				this.curr = this.curr.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}
		
	}
	
	private Node<E> dummy;
	private int size;

	public SinglyLinkedList() {
		dummy = new Node<E>();
		dummy.setElement(null);
		size = 0;
	}

	@Override
	public Iterator<E> iterator() {
		return new SinglyLinkedListIterator<E>();
	}

	@Override
	public void add(E obj) {
		if(isEmpty()) {
			Node<E> newN = new Node<E>(obj, null);
			dummy.setNext(newN);
		}
		else {
			Node<E> curr = dummy.getNext();
			while(curr.getNext() != null) {
				curr = curr.getNext();
			}
			Node<E> newN = new Node<E>(obj, null);
			curr.setNext(newN);
		}
		this.size++;
	}

	@Override
	public void add(int index, E obj) {
		if(index == this.size || index == this.size) {
			this.add(obj);
			return;
		}
		if(index == 0) {
			Node<E> newN = new Node<E>(obj, dummy.getNext());
			dummy.setNext(newN);
		}
		else {
			int i = 0;
			Node<E> curr = dummy.getNext();
			while(i < index-1) {
				curr = curr.getNext();
				i++;
			}
			Node<E> newN = new Node<E>(obj, null);
			newN.setNext(curr.getNext());
			curr.setNext(newN);
		}
		this.size++;
	}

	@Override
	public boolean remove(E obj) {
		Node<E> curr = dummy.getNext();
		if(curr.getElement() == obj) {
			dummy.setNext(curr.getNext());
			curr.setElement(null);
			curr.setNext(null);
			this.size--;
			return true;
		}
		while(curr.getNext().getElement() != obj) {
			curr = curr.getNext();
		}
		Node<E> target = curr.getNext();
		curr.setNext(target.getNext());
		target.setElement(null);
		this.size--;
		return true;
	}

	@Override
	public boolean remove(int index) {
		if(index < 0 || index > this.size) {
			throw new IndexOutOfBoundsException("Index is invalid");
		}
		if(index == 0) {
			Node<E> temp = dummy.getNext();
			dummy.setNext(temp.getNext());
			temp.setElement(null);
			temp.setNext(null);
			this.size--;
			return true;
		}
		else {
			Node<E> node1 = this.findNode(index);
			Node<E> node2 = node1.getNext();
			node1.setNext(node2.getNext());
			node2.setNext(null);
			node2.setElement(null);
			this.size--;
			return true;
		}
	}

	@Override
	public int removeAll(E obj) {
		if(this.isEmpty()) {
			return 0;
		}
		int countRemoved = 0;
		Node<E> curr = dummy.getNext();
		while(curr != null) {
			if(curr.getElement() == obj) {
				remove(obj);
				countRemoved++;
			}
			curr = curr.getNext();
		}
		return countRemoved;
	}

	@Override
	public E get(int index) {
		if(index < 0 || index >= this.size) {
			throw new IndexOutOfBoundsException("Index is invalid");
		}
		Node<E> curr = dummy.getNext();
		int i = 0;
		while(i != index) {
			curr = curr.getNext();
			i++;
		}
		return curr.getElement();
	}

	@Override
	public E set(int index, E obj) {
		int i = 0;
		Node<E> curr = dummy.getNext();
		while(i<index) {
			curr = curr.getNext();
			i++;
		}
		E etr = curr.getElement();
		curr.setElement(obj);
		return etr;
	}

	@Override
	public E first() {
		if(isEmpty()) {
			return null;
		}
		return dummy.getNext().getElement();
	}

	@Override
	public E last() {
		if(isEmpty()) {
			return null;
		}
		Node<E> curr = dummy.getNext();
		while(curr.getNext() != null) {
			curr = curr.getNext();
		}
		return curr.getElement();
	}

	@Override
	public int firstIndex(E obj) {
		int index = 0;
		Node<E> curr = dummy.getNext();
		while(curr != null) {
			if(curr.getElement()==obj) {
				return index;
			}
			index++;
			curr = curr.getNext();
		}
		return -1;
	}

	@Override
	public int lastIndex(E obj) {
		Node<E> curr = this.dummy.getNext();
		int index = -1;
		for(int i = 0; i<this.size; i++) {
			if(curr.getElement() == obj) {
				index = i;
			}
			curr = curr.getNext();
		}
		return index;
	}

	@Override
	public int size() {
		return this.size;
	}

	@Override
	public boolean isEmpty() {
		return this.size==0;
	}

	@Override
	public boolean contains(E obj) {
		Node<E> curr = dummy.getNext();
		while(curr != null) {
			if(curr.getElement() == obj) {
				return true;
			}
			curr = curr.getNext();
		}
		return false;
	}

	@Override
	public void clear() {
		Node<E> curr = dummy.getNext();
		while(curr != null) {
			curr.setElement(null);
			curr = curr.getNext();
		}
		
	}
	
	private Node<E> findNode(int index) {
		Node<E> temp = this.dummy;
		int i = 0;
		
		while (i < index) {
			temp = temp.getNext();
			i++;
		}
		return temp;
		
	}

	private static class Node<E> {
		private E element;
		private Node<E> next;
		public Node() {
			element = null;
			next = null;
		}

		public Node(E element, Node<E> next) {
			this.element = element;
			this.next = next;
		}
		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
		public Node<E> getNext() {
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}
	}

}
