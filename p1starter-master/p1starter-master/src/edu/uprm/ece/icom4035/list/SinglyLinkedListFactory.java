package edu.uprm.ece.icom4035.list;

public class SinglyLinkedListFactory<E> implements ListFactory<E> {
	
	public List<E> getList(String criteria) {
		if(criteria.equals("SinglyLinkedList")) {
			return newInstance();
		}
		return null;
	}

	@Override
	public List<E> newInstance() {
		return new SinglyLinkedList<E>();
	}
	
}
