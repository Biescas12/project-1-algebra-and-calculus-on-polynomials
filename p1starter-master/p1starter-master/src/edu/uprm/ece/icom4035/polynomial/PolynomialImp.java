package edu.uprm.ece.icom4035.polynomial;


import java.util.Iterator;
import edu.uprm.ece.icom4035.list.*;
import edu.uprm.ece.icom4035.list.ListFactory;

public class PolynomialImp implements Polynomial {

	private String str;
	private List<Term> terminos;

	public PolynomialImp() {
		terminos = TermListFactory.newListFactory().newInstance();
	}

	public PolynomialImp(String string) { //Takes Polynomial String and creates each term and then adds it to the terminos list.
		this.str = string;
		terminos = TermListFactory.newListFactory().newInstance();
		String[] arrStr = string.split("\\+");
		for(int i = 0; i<arrStr.length; i++) {
			if(arrStr[i].contains("x")) {
				if(arrStr[i].contains("^")) {
					double coef = 0;
					int exp = 0;
					TermImp term = new TermImp(coef, exp);
					if(arrStr[i].substring(0, arrStr[i].indexOf("x")).isEmpty()) {
						coef = 1;
					}
					else if(arrStr[i].substring(0, arrStr[i].indexOf("x")).equals("-")) {
						coef = -1;
					}
					else {
						coef = Double.valueOf(arrStr[i].substring(0, arrStr[i].indexOf("x")));
					}
					exp = Integer.valueOf(arrStr[i].substring(arrStr[i].indexOf("^")+1, arrStr[i].length()));
					term = new TermImp(coef, exp);
					terminos.add(term);
				}
				else {
					double coef = 0;
					int exp = 0;
					TermImp term = new TermImp(coef, exp);
					if(arrStr[i].substring(0, arrStr[i].indexOf("x")).isEmpty()) {
						coef = 1;
					}
					else if(arrStr[i].substring(0, arrStr[i].indexOf("x")).equals("-")) {
						coef = -1;

					}
					else {
						coef = Double.valueOf(arrStr[i].substring(0, arrStr[i].indexOf("x"))); 
					}
					exp = 1;
					term = new TermImp(coef, exp);
					terminos.add(term);
				}
			}
			else {
				double coef = Double.valueOf(arrStr[i]);
				int exp = 0;
				TermImp term = new TermImp(coef, exp);
				terminos.add(term);
			}
		}

	}

	@Override
	public Iterator<Term> iterator() {
		return terminos.iterator();
	}

	@Override
	public Polynomial add(Polynomial P2) {
		List<Term> sum = TermListFactory.newListFactory().newInstance();
		List<Term> p2Term = TermListFactory.newListFactory().newInstance();
		PolynomialImp result = new PolynomialImp();
		PolynomialImp p2Imp = (PolynomialImp) P2;
		p2Term = p2Imp.terminos;
		if(p2Term.size() > this.terminos.size()) {
			for(int i = 0; i < terminos.size();) {
				for(int j = 0; j < p2Term.size();) {

					if(i <this.terminos.size() && j< p2Term.size()) {
						if(this.terminos.get(i).getExponent() == p2Term.get(j).getExponent()) {

							sum.add(new TermImp(terminos.get(i).getCoefficient() + p2Term.get(j).getCoefficient(), 
									terminos.get(i).getExponent()));
							i++;
							j++;
						}
						else if(terminos.get(i).getExponent() > p2Term.get(j).getExponent()) {
							sum.add(this.terminos.get(i));
							i++;
						}
						else  {
							sum.add(p2Term.get(j));
							j++;
						}
					}
					else {
						if(i == this.terminos.size()) {
							sum.add(p2Term.get(j));
							j++;
						}
						else if(j == p2Term.size()) {
							sum.add(this.terminos.get(i));
							i++;
						}
					}
				}
			}
		}
		else {
			for(int i = 0; i < p2Term.size();) {
				for(int j = 0; j < this.terminos.size();) {

					if(j <this.terminos.size() && i < p2Term.size()) {
						if(this.terminos.get(j).getExponent() == p2Term.get(i).getExponent()) {
							sum.add(new TermImp(terminos.get(j).getCoefficient() + p2Term.get(i).getCoefficient(), 
									p2Term.get(i).getExponent()));
							i++;
							j++;
						}
						else if(p2Term.get(i).getExponent() > this.terminos.get(j).getExponent()) {
							sum.add(p2Term.get(i));
							i++;
						}
						else  {
							sum.add(this.terminos.get(j));
							j++;
						}
					}
					else {
						if(i == p2Term.size()) {
							sum.add(this.terminos.get(j));
							j++;
						}
						else if(j == this.terminos.size()) {
							sum.add(p2Term.get(i));
							i++;
						}
					}
				}
			}
		}

		for(int ind = 0; ind < sum.size(); ind++) {
			if(sum.get(ind).getCoefficient() == 0) {
				sum.remove(ind);
				ind--;
			}
			else {
				result.addTermino(sum.get(ind));
			}
		}
		return result;
	}

	private void addTermino(Term nextTerm) {
		this.terminos.add(nextTerm);
	}

	@Override
	public Polynomial subtract(Polynomial P2) {
		if(this.equals(P2)) {
			return new PolynomialImp("0");
		}
		P2 = P2.multiply(-1);
		return this.add(P2);
	}

	@Override
	public Polynomial multiply(Polynomial P2) {
		List<Term> multi = TermListFactory.newListFactory().newInstance();
		List<Term> p2Term = TermListFactory.newListFactory().newInstance();
		PolynomialImp result = new PolynomialImp();
		PolynomialImp p2Imp = (PolynomialImp) P2;
		p2Term = p2Imp.terminos;
		if(P2.toString().equals("0.00")) {
			return new PolynomialImp("0");
		}
		for(int i = 0; i < this.terminos.size(); i++) {
			for(int j = 0; j < p2Term.size(); j++) {
				multi.add(new TermImp(this.terminos.get(i).getCoefficient()*p2Term.get(j).getCoefficient(), 
						this.terminos.get(i).getExponent() + p2Term.get(j).getExponent()));
			}
		}
		Term term = null;
		for(int i = 0; i < multi.size();i++) {
			for(int j = i + 1; j <multi.size() ;j++) {
				if(multi.get(i).getExponent() < multi.get(j).getExponent()) {
					term = multi.get(i);
					multi.set(i, new TermImp(multi.get(j).getCoefficient(), multi.get(j).getExponent()));
					multi.set(j, term);
				}
				if(multi.get(i).getExponent() == multi.get(j).getExponent()) {
					multi.set(i, new TermImp(multi.get(i).getCoefficient() + multi.get(j).getCoefficient(), 
							multi.get(i).getExponent()));
					multi.remove(j);
					j--;
				}
			}
		}
		for(int ind = 0; ind < multi.size(); ind++) {
			if(multi.get(ind).getCoefficient() == 0) {
				multi.remove(ind);
				ind--;
			}
			else {
				result.addTermino(multi.get(ind));
			}
		}
		return result;
	}

	@Override
	public Polynomial multiply(double c) {
		List<Term> neg = TermListFactory.newListFactory().newInstance();
		PolynomialImp result = new PolynomialImp();
		if(c == 0) {
			return new PolynomialImp("0");
		}
		for(int i = 0; i < this.terminos.size(); i++) {
			double newNum = this.terminos.get(i).getCoefficient() * c;
			neg.add(new TermImp(newNum, this.terminos.get(i).getExponent()));
		}
		for(int ind = 0; ind < neg.size(); ind++) {
			if(neg.get(ind).getCoefficient() == 0) {
				neg.remove(ind);
				ind--;
			}
			else {
				result.addTermino(neg.get(ind));
			}
		}
		return result;
	}

	@Override
	public Polynomial derivative() {
		List<Term> deriv = TermListFactory.newListFactory().newInstance();
		PolynomialImp result = new PolynomialImp();	
		for(int i = 0; i < this.terminos.size(); i++) {
			double derivCoef = this.terminos.get(i).getCoefficient()*this.terminos.get(i).getExponent();
			int newExp = this.terminos.get(i).getExponent() - 1;
			deriv.add(new TermImp(derivCoef,newExp));
		}
		for(int ind = 0; ind < deriv.size(); ind++) {
			if(deriv.get(ind).getCoefficient() == 0) {
				deriv.remove(ind);
				ind--;
			}
			else {
				result.addTermino(deriv.get(ind));
			}
		}
		return result;	
	}

	@Override
	public Polynomial indefiniteIntegral() {
		List<Term> integral = TermListFactory.newListFactory().newInstance();
		PolynomialImp result = new PolynomialImp();
		for(int i = 0; i <this.terminos.size(); i++) {
			int newExp = this.terminos.get(i).getExponent()+1;
			double integCoef = this.terminos.get(i).getCoefficient() / (this.terminos.get(i).getExponent() + 1);
			integral.add(new TermImp(integCoef, newExp));
		}
		integral.add(new TermImp(1, 0));
		for(int ind = 0; ind < integral.size(); ind++) {
			if(integral.get(ind).getCoefficient() == 0) {
				integral.remove(ind);
				ind--;
			}
			else {
				result.addTermino(integral.get(ind));
			}
		}
		return result;	
	}

	@Override
	public double definiteIntegral(double a, double b) {
		Polynomial result = new PolynomialImp();
		result = this.indefiniteIntegral();
		double evalA = result.evaluate(a);
		double evalB = result.evaluate(b);
		double resultInt = evalB - evalA;
		return resultInt;
	}

	@Override
	public int degree() {
		int degree = this.terminos.first().getExponent();
		return degree;
	}

	@Override
	public double evaluate(double x) {
		double eval = 0;
		for(int i = 0; i <this.terminos.size(); i++) {
			eval = eval + this.terminos.get(i).getCoefficient() * Math.pow(x, this.terminos.get(i).getExponent());
		}
		return eval;
	}

	@Override
	public boolean equals(Polynomial P) {
		List<Term> p2Term = TermListFactory.newListFactory().newInstance();
		PolynomialImp p2Imp = (PolynomialImp) P;
		p2Term = p2Imp.terminos;
		if(this.terminos.size() > p2Term.size() || this.terminos.size() < p2Term.size()) {
			return false;
		}
		for(int i = 0; i<this.terminos.size(); i++) {
			if(p2Term.get(i).getCoefficient() != this.terminos.get(i).getCoefficient() || 
					p2Term.get(i).getExponent() != this.terminos.get(i).getExponent()) {
				return false;
			}
		}
		return true;
	}

	public String toString() { //Converts Polynomials into Strings
		String str = "";
		if(!this.terminos.isEmpty()) {
			for(Term obj : this) {
				if(obj.getExponent() == 0) {
					str += String.format("%.2f", obj.getCoefficient());
				}
				else if(obj.getExponent() == 1) {
					str += String.format("%.2f", obj.getCoefficient()) + "x" + "+";
				}
				else {
					if(obj.getCoefficient() == 1) {
						str += "x^" + obj.getExponent() + "+";
					}
					else if(obj.getCoefficient() == -1) {
						str += "-x^" + obj.getExponent() + "+";
					}
					else {
						str += String.format("%.2f", obj.getCoefficient()) + "x^" + obj.getExponent() + "+";
					}
				}
			}
			if(str.lastIndexOf("+") == str.length()-1) {
				str = str.substring(0, str.length()-1);
			}
			return str;
		}
		else {
			return "0.00";
		}
	}
}
