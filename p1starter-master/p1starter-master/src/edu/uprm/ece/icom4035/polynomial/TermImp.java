package edu.uprm.ece.icom4035.polynomial;

public class TermImp implements Term {
	private double coefficient;
	private int exponent;
	
	public TermImp() {
		this.coefficient = 0;
		this.exponent = 0;
	}
	
	public TermImp(double coe, int exp) {
		this.coefficient = coe;
		this.exponent = exp;
	}

	@Override
	public double getCoefficient() {
		return this.coefficient;
	}

	@Override
	public int getExponent() {
		return this.exponent;
	}

	@Override
	public double evaluate(double x) {
		double eval = 0;
		eval = this.coefficient*Math.pow(x, this.exponent);
		return eval;
	}

}
